using Combat;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.AI;

namespace SpeediesAttack
{
    public class Speedy : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private Transform T;
        [SerializeField] private NavMeshAgent agent;
        [SerializeField] private Animator animator;
        [SerializeField] private Punch punch;
        [SerializeField] private Punch kick;

        [Header("Movement")]
        [Range(0f, 1f)][SerializeField] private float safeZone;
        [SerializeField] private float attackRange;

        [Header("Attacks")]
        [SerializeField] private float attackSpeed;
        /// <summary> kick every X punshes </summary>
        [SerializeField] private int kicksPeriod;
        private float counter;
        private Transform target;
        private bool isRunning;
        private float distance;
        private int attackCounter;

        [Header("VFX")]
        [SerializeField] private ParticleSystem punchVFX;
        [SerializeField] private ParticleSystem kickVFX;
        [SerializeField] private GameObject explosion;

        [Header("Health")]
        [SerializeField] private Health health;
        public bool isDead { get; private set; }

        public void SetParameters(int punchDamage, int punchCriticalDamage, int punchCriticalChance, int kickDamage, 
            int kickCriticalDamage, int kickCriticalChance)
        {
            punch.Setup(punchDamage, punchCriticalDamage, punchCriticalChance);
            kick.Setup(kickDamage, kickCriticalDamage, kickCriticalChance);
        }

        void Start()
        {
            SetTarget(Player.PlayerController.I.T);
            health.Setup(OnDeath);
            health.SetParent(ScreensManager.I.Canvas3D_T);
        }

        private void OnEnable()
        {
            Initialize();
        }

        private void OnDisable()
        {
            health.EnableBar(false);
        }

        void Update()
        {
            distance = Vector3.Distance(T.position, target.position);
            if (isRunning)
            {
                agent.SetDestination(target.position);
                if (distance < safeZone)
                {
                    isRunning = false;
                    animator.SetBool("running", false);
                    agent.isStopped = true;
                }
            }
            else
            {
                if (distance > attackRange)
                {
                    isRunning = true;
                    animator.SetBool("running", true);
                    agent.isStopped = false;
                }
                else
                {
                    Vector3 sameLevelTarget = target.position;
                    sameLevelTarget.y = T.position.y;
                    if (distance > 0.05f) T.LookAt(sameLevelTarget);
                }
            }

            if (distance < attackRange)
            {
                counter -= Time.deltaTime;
                if (counter < 0)
                {
                    counter = attackSpeed;
                    if (attackCounter >= kicksPeriod && !isRunning)
                    {
                        Kick();
                    }
                    else Punsh();
                }
            }
        }

        private void Initialize()
        {
            isDead = false;
            health.Initialize();
            health.EnableBar(true);
            isRunning = false;
            counter = 0;
            attackCounter = 0;
        }

        public void OnDeath()
        {
            gameObject.SetActive(false);
            isDead = true;
            Destroy(Instantiate(explosion, T.position, T.rotation), 4);
        }

        private void Punsh()
        {
            attackCounter++;
            animator.SetTrigger("punch");
        }

        private void Kick()
        {
            attackCounter = 0;
            animator.SetTrigger("kick");
        }

        public void SetTarget(Transform target)
        {
            this.target = target;
        }

        public void ANIM_Kick()
        {
            kick.Execute();
        }

        public void ANIM_Punch()
        {
            punch.Execute();
        }

        private void OnDrawGizmosSelected()
        {
            if (T == null) return;

            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(T.position, attackRange);
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(T.position, attackRange * safeZone);
        }
    }
}