using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpeediesAttack
{
    public class SpawnCircle : MonoBehaviour
    {
        public Speedy speedy;
        [SerializeField] private GameObject explosionVFX;
        [SerializeField] private ParticleSystem ruinsVFX;
        [SerializeField] private GameObject light;
        private float counter;
        private bool isCharging;
        public bool hasSpawnedSpeedy { get; private set; }

        private void Update()
        {
            if (isCharging)
            {
                counter -= Time.deltaTime;
                if (counter < 0)
                {
                    if (!hasSpawnedSpeedy)
                    {
                        Debug.Log("Finished animation");
                        ruinsVFX.Stop();
                        hasSpawnedSpeedy = true;
                        speedy.transform.position = transform.position;
                        explosionVFX.SetActive(true);
                        speedy.gameObject.SetActive(true);
                        light.SetActive(false);
                        counter = 5;
                        gameObject.SetActive(false);
                    }
                }
            }
        }

        private void Initialize()
        {
            Debug.Log("Circle is initialized");
            speedy.gameObject.SetActive(false);
            gameObject.SetActive(true);
            light.SetActive(true);
            explosionVFX.SetActive(false);
            hasSpawnedSpeedy = false;
            ruinsVFX.Play();
            isCharging = false;
        }

        public void StartCharging(float chargingTime)
        {
            Initialize();
            isCharging = true;
            counter = chargingTime;
        }

        public void Terminate()
        {
            gameObject.SetActive(false);
        }
    }
}