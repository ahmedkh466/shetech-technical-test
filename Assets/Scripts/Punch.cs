using Unity.Burst.CompilerServices;
using UnityEngine;

namespace Combat
{
    public class Punch : MonoBehaviour
    {
        [Header("Effects")]
        [SerializeField] private ParticleSystem vfx;

        [Header("Collider")]
        [SerializeField] private Transform colliderOriginT;
        [SerializeField] private float radius;
        [SerializeField] private LayerMask layer;
        private Collider[] hits;

        [Header("Damage")]
        [SerializeField] private int damage;
        [SerializeField] private float criticalDamage;
        [Range(0, 100)]
        [SerializeField] private int criticalChance;

        [Header("Editor")]
        [SerializeField] private bool visualizeCollider;

        public void Setup(int damage, int criticalDamage, int criticalChance)
        {
            this.damage = damage;
            this.criticalDamage = criticalDamage;
            this.criticalChance = criticalChance;
        }

        /// <summary>
        /// </summary>
        /// <returns>Damage dealt</returns>
        public int Execute()
        {
            vfx.Play();
            hits = Physics.OverlapSphere(colliderOriginT.position, radius,layer, QueryTriggerInteraction.Collide);
            if (hits.Length != 0)
            {
                int totalDamage = 0;
                foreach (Collider hit in hits)
                {
                    int criticalDmg = (int)(Random.Range(0, 101) <= criticalChance ? criticalDamage : 0);
                    int finalDamage = criticalDmg + damage + criticalDmg + (int)(damage * 0.2f * Random.Range(-1f, 1f));
                    hit.GetComponent<Health>().TakeDamage(finalDamage);

                    totalDamage += finalDamage;
                }
                return totalDamage;
            }
            else return -1;
        }

        private void OnDrawGizmos()
        {
            if (!visualizeCollider) return;
            if (colliderOriginT == null) return;
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(colliderOriginT.position, radius);
        }
    }
}