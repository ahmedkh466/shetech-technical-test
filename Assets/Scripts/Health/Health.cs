using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    [SerializeField] private int maxHealth;
    public int health { get; private set; }

    [Header("UI")]
    [SerializeField] private GameObject healthBarPanel;
    [SerializeField] private Image bar;
    [SerializeField] private float lerpSpeed = 15;

    private UnityAction deathCall;
    private UnityAction hitCall;

    public void Initialize()
    {
        health = maxHealth;
        bar.fillAmount = 0;
    }

    public void Setup(UnityAction deathCall, UnityAction hitCall)
    {
        this.deathCall = deathCall;
        this.hitCall = hitCall;
    }

    public void Setup(UnityAction deathCall)
    {
        this.deathCall = deathCall;
    }

    public void SetMaxHealth(int maxHp)
    {
        maxHealth = maxHp;
    }

    public void TakeDamage(int damage)
    {
        if(hitCall != null)
        hitCall();

        if (health == 0) return;

        health -= damage;

        if (health <= 0)
        {
            health = 0;
            if (deathCall != null)
                deathCall();
        }
    }

    private void Update()
    {
        bar.fillAmount = Mathf.Lerp(bar.fillAmount, health / (float)maxHealth, Time.deltaTime * lerpSpeed);
    }

    public void EnableBar(bool enable)
    {
        if (healthBarPanel != null)//editor problem
            healthBarPanel.SetActive(enable);
    }

    public void SetParent(Transform parentT)
    {
        healthBarPanel.transform.SetParent(parentT);
    }

    public void Heal(int bonus)
    {
        health += bonus;
    }
}
