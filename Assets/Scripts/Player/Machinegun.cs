using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameObjectPooling;

namespace Player {
    public class Machinegun : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private PlayerController playerController;
        [SerializeField] private Transform cameraT;
        [SerializeField] private Camera camera;

        [Header("Shooting")]
        [SerializeField] private Pool bulletsPool;
        [SerializeField] private float attackSpeed;
        [SerializeField] private Transform bulletSpawnT;
        [SerializeField] private LayerMask targetsLayer;
        private bool isShooting;
        private Ray ray;
        private RaycastHit hit;

        [Header("Effects")]
        [SerializeField] private ParticleSystem nozzleEffect;
        private float counter;


        private void Update()
        {
            if (isShooting)
            {
                if (playerController.currentState == PlayerController.State.rolling)
                {
                    isShooting = false;
                    return;
                }

                ray = camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 9000, targetsLayer))
                {
                    if(Vector3.Distance(hit.point, bulletSpawnT.position) > 5)
                        bulletSpawnT.rotation = Quaternion.LookRotation(hit.point - bulletSpawnT.position);
                    else bulletSpawnT.rotation = cameraT.rotation;
                }
                else
                    bulletSpawnT.rotation = cameraT.rotation;

                counter -= Time.deltaTime;
                if (counter < 0)
                {
                    counter = attackSpeed;
                    ShootBullet();
                }
            }
        }

        public void Input_StartShooting()
        {
            if(isShooting) return;

            if (playerController.currentState != PlayerController.State.rolling)
            {
                isShooting = true;
            }
        }

        public void Input_StopShooting()
        {
            if (isShooting) isShooting = false;
        }

        private void ShootBullet()
        {
            nozzleEffect.Play();
            bulletsPool.SpawnEntity<Transform>(bulletSpawnT.position, bulletSpawnT.rotation);
        }
    }
}