using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        public enum State
        {
            idle,
            running,
            rolling,
            inAir
        }

        public static PlayerController I;

        public State currentState { get; private set; }

        [Header("References")]
        public Transform T;
        public Transform heartT;
        [SerializeField] private Transform cameraTargetT;
        [SerializeField] private Transform cameraT;
        [SerializeField] private CharacterController characterController;
        [SerializeField] private Animator animator;
        [SerializeField] private CinemachineVirtualCamera virtualCamera;
        private AnimatorOverrideController animatorOverrideController;

        [Header("Camera")]
        [SerializeField] private Vector2 rotationSpeed;
        [SerializeField] private LayerMask cameraObstacles;
        [SerializeField] private Vector3 cameraTargetOffset; //the offset to player
        private float yAngle;
        private float xAngle;

        [Header("Movement")]
        [SerializeField] private float movementSpeed;
        [SerializeField] private float rotationSmooth;
        [Tooltip("Time it takes to count as falling")]
        [SerializeField] private float fallingTimeout;
        private float fallingTimer;
        private float verticalVelocity; //y axis

        [Header("Jumping")]
        [SerializeField] private float jumpForce;
        [SerializeField] private float groundCheckDistance;
        [SerializeField] private float groundCheckRadius;
        [SerializeField] private LayerMask walkableLayers;
        [SerializeField] private float gravity;
        private bool isGrounded;
        private bool jumped = false;

        [Header("Rolling")]
        [SerializeField] private float rollSpeed;
        [SerializeField] private float rollRotationSmooth;
        private Timer rollTimer = new Timer();

        [Header("Input")]
        public Vector2 movement;
        public Vector2 mouse;
        public bool jump;
        public bool roll;

        private void Awake()
        {
            I = this;
        }

        private void FixedUpdate()
        {
            if (currentState == State.rolling)
            {
                HandleRolling(movement);
            }
            else
            {
                Move(movement);
            }

            Fall();
        }

        private void Update()
        {
            cameraTargetT.position = T.position + cameraTargetOffset;

            if (jump)
            {
                Jump();
                jump = false;
            }
            else if (roll)
            {
                Roll();
                roll = false;
            }
        }

        private void LateUpdate()
        {
            LookAround(mouse);
        }

        #region Camera
        public void LookAround(Vector2 input)
        {
            yAngle += input.x * rotationSpeed.x * Time.fixedDeltaTime;
            xAngle += input.y * rotationSpeed.y * Time.fixedDeltaTime;
            xAngle = Mathf.Clamp(xAngle, -50, 60);
            cameraTargetT.localRotation = Quaternion.Euler(xAngle, yAngle, 0);
        }
        #endregion

        #region Movement
        private void HandleRolling(Vector2 input)
        {
            if (rollTimer.IsFinished())
            {
                SetState(movement == Vector2.zero ? State.idle : (currentState == State.inAir ? State.idle : State.running));
            }
            else
            {
                characterController.Move(T.forward * Time.fixedDeltaTime * rollSpeed);
            }
        }

        private void SetState(State state)
        {
            if (state == currentState) return;

            switch (currentState)
            {
                case State.running:
                animator.SetBool("running", false);
                    break;
            }

            currentState = state;

            switch (state)
            {
                case State.running:
                    animator.SetBool("running", true);
                    break;
                case State.rolling:
                    animator.SetTrigger("roll");
                    break;
                case State.idle:
                    animator.SetBool("running", false);
                    break;
                case State.inAir:
                    break;
            }
        }

        private void Roll()
        {
            if (currentState == State.rolling || currentState == State.inAir) return;
            SetState(State.rolling);

            rollTimer.StartCounting(1);

        }

        private void Move(Vector2 input)
        {
            if (input == Vector2.zero) //stop running
            {
                if(currentState == State.running) SetState(State.idle);
                return;
            }

            //rotation
            Vector3 cameraForward = new Vector3(cameraTargetT.forward.x, 0, cameraTargetT.forward.z).normalized;
            T.rotation = Quaternion.Lerp(T.rotation, Quaternion.LookRotation(cameraForward * input.y + cameraTargetT.right * input.x), Time.fixedDeltaTime * rotationSmooth);
            
            //movement
            Vector3 movementDirection = (cameraForward * input.y + cameraTargetT.right * input.x).normalized;
            characterController.Move(movementDirection * movementSpeed * Time.fixedDeltaTime);

            //animation
            if (currentState != State.inAir && currentState != State.rolling)
            {
                SetState(State.running);
            }
        }

        public void Jump()
        {
            if (currentState == State.inAir || currentState == State.rolling) return;
            verticalVelocity = jumpForce;
            animator.SetTrigger("jump"); //animator has to be informed earlier of jumping to take into account whether the character is running or not
            SetState(State.inAir);
            jumped = true;
        }

        private void Fall()
        {
            isGrounded = Physics.CheckSphere(T.position + Vector3.down * groundCheckDistance, groundCheckRadius, walkableLayers, QueryTriggerInteraction.Ignore);
            if (!isGrounded)
            {
                verticalVelocity -= gravity * Time.fixedDeltaTime;
                if(currentState != State.inAir)
                {
                    if (fallingTimer == 0)
                    {
                        fallingTimer = fallingTimeout;
                    }
                    else if(fallingTimer > 0)
                    {
                        fallingTimer -= Time.deltaTime;
                    }
                    else
                    {
                        fallingTimer = 0;
                        SetState(State.inAir);
                    }
                }
            }
            else
            {
                if(fallingTimer != 0) fallingTimer = 0;

                if (verticalVelocity < 0)
                {
                    verticalVelocity = 0;
                    if (currentState == State.inAir)
                    {
                        SetState(movement == Vector2.zero ? State.idle : State.running);
                    }
                }
            }
            animator.SetBool("isGrounded", currentState != State.inAir);

            if (verticalVelocity != 0)
            {
                characterController.Move(verticalVelocity * Time.fixedDeltaTime * Vector3.up);
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (Application.isPlaying) return;

            isGrounded = Physics.CheckSphere(T.position + Vector3.down * groundCheckDistance, groundCheckRadius, walkableLayers);
            Gizmos.color = isGrounded ? Color.green : Color.red;
            Gizmos.DrawSphere(T.position + Vector3.down * groundCheckDistance, groundCheckRadius);

            Gizmos.color = Color.blue;
            cameraTargetT.position = cameraTargetOffset + T.position;
            Gizmos.DrawWireCube(cameraTargetT.position, Vector3.one * 0.4f);
        }
        #endregion
    }
}