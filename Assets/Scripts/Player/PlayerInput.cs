using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] private PlayerController player;
        private Vector2 movementDirection;
        private Vector2 lookingInput;

        private void Update()
        {
            movementDirection.x = Input.GetAxisRaw("Horizontal");
            movementDirection.y = Input.GetAxisRaw("Vertical");
            lookingInput.x = Input.GetAxis("Mouse X");
            lookingInput.y = -Input.GetAxis("Mouse Y");
            player.movement = movementDirection;
            player.mouse = lookingInput;
            if (Input.GetButtonDown("Roll"))
            {
                player.roll = true;
            }
            else if (Input.GetButtonDown("Jump"))
            {
                player.jump = true;
            }
        }
    }
}