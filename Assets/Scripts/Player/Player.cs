using Combat;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;

namespace Player {
    public class Player : MonoBehaviour
    {
        public static Player I;
        public static bool immortal;
        private bool hasDied;

        public LayerMask layer;

        [SerializeField] private int maxHealth;
        [SerializeField] private Health health;

        [Header("Camera")]
        public Transform cameraT;
        [SerializeField] private Vector3 regularShoulderOffset;
        [SerializeField] private Vector3 aimShoulderOffset;
        [SerializeField] private float shoulderOffsetDamp;
        [SerializeField] private CinemachineVirtualCamera virtualCamera;
        private Cinemachine3rdPersonFollow thirdPersonCamera;
        private Vector3 targetShoulderOffset;

        [Header("Offense")]
        [SerializeField] private Machinegun machinegun;
        [SerializeField] private Punch punch;
        [SerializeField] private float attackSpeed;
        [Range(0f, 1f)]
        [SerializeField] private float lifesteal;
        private float counter;

        [Header("UI")]
        [SerializeField] private GameObject crossHair;

        [Header("Post Processing")]
        [SerializeField] private Volume pp;
        [Range(0f, 1f)] [SerializeField] private float damage_vignetteAddition;
        [Range(-1f, 1f)] [SerializeField] private float damage_lensDistortion;
        private float initialVignette;
        private Vignette vignette;
        private ChromaticAberration chromaticAbberation;
        private LensDistortion lensDistortion;

        private void Awake()
        {
            I = this;

            thirdPersonCamera = virtualCamera.GetCinemachineComponent<Cinemachine3rdPersonFollow>();

            //health
            health.SetMaxHealth(maxHealth);
            health.Setup(OnGetKilled, OnGetHit);
            health.Initialize();

            //post processing
            pp.profile.TryGet<Vignette>(out vignette);
            pp.profile.TryGet<ChromaticAberration>(out chromaticAbberation);
            pp.profile.TryGet<LensDistortion>(out lensDistortion);
            initialVignette = vignette.intensity.value;
            immortal = false;
        }

        private void Update()
        {
            thirdPersonCamera.ShoulderOffset = Vector3.Lerp(thirdPersonCamera.ShoulderOffset, targetShoulderOffset, Time.deltaTime * shoulderOffsetDamp);
            if (Input.GetMouseButtonDown(0))
            {
                targetShoulderOffset = aimShoulderOffset;
                crossHair.SetActive(true);
            }
            else if (Input.GetMouseButton(0))
            {
                machinegun.Input_StartShooting();
            }
            else if (Input.GetMouseButtonUp(0))
            {
                machinegun.Input_StopShooting();
                targetShoulderOffset = regularShoulderOffset;
                crossHair.SetActive(false);
            }
            if (Input.GetMouseButtonDown(1))
            {
                if (counter <= 0)
                {
                    int damage = punch.Execute(); 
                    if (damage != -1)
                    {
                        LifeSteal(damage);
                    }
                    counter = attackSpeed;
                }
            }

            if(counter>0)
                counter -= Time.deltaTime;
        }

        public void OnGetKilled()
        {
            if (!hasDied)
            {
                ScreensManager.I.DisplayDeathMessage();
                if (!immortal)
                {
                    ScreensManager.I.Pause(true);
                    hasDied = true;
                }
            }
        }

        public void OnGetHit()
        {
            float ratio = 1 - (health.health / (float)maxHealth);
            vignette.intensity.value = initialVignette + ratio * damage_vignetteAddition;
            chromaticAbberation.intensity.value = ratio;
            if (ratio > 0.6f) lensDistortion.intensity.value = damage_lensDistortion * ((1f - ratio) / 0.4f);
            else if (lensDistortion.intensity.value != 0) lensDistortion.intensity.value = 0;
        }

        public void LifeSteal(int damage)
        {
            health.Heal((int)(damage * lifesteal));
        }
    }
}