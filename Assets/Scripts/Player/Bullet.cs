using UnityEngine;

namespace Player
{
    public class Bullet : MonoBehaviour
    {
        private Transform T;
        [SerializeField] private float speed;
        private float deathCounter = 10;

        private void Start()
        {
            T = transform;
        }

        private void OnEnable()
        {
            deathCounter = 10;
        }

        private void Update()
        {
            deathCounter -= Time.deltaTime;
            if (deathCounter < 0)
            {
                gameObject.SetActive(false);
            }
        }

        private void FixedUpdate()
        {
            T.position += T.forward * speed * Time.fixedDeltaTime;
        }

        private void OnCollisionEnter(Collision collision)
        {
            gameObject.SetActive(false);
            if (collision.gameObject.CompareTag("Enemy"))
            {
                collision.gameObject.GetComponent<Health>().TakeDamage(20);
                Player.I.LifeSteal(20);
            }
        }
    }
}