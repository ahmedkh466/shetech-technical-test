using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScreensManager : MonoBehaviour
{
    public static ScreensManager I;

    [SerializeField] private GameObject idleScreen;
    [SerializeField] private GameObject pauseScreen;
    [SerializeField] private GameObject deathMessage;
    [SerializeField] private Toggle immortalityToggle;
    [SerializeField] private GameObject startingMessage;
    private bool isPaused = false;

    [Header("Canvases")]
    public Transform Canvas3D_T;

    private void Awake()
    {
        I = this;
        LockCursor(true);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause(!isPaused);
        }
    }

    public void Pause(bool pause)
    {
        if (startingMessage.activeInHierarchy) startingMessage.SetActive(false);

        isPaused = pause;
        idleScreen.SetActive(!isPaused);
        pauseScreen.SetActive(isPaused);
        Time.timeScale = isPaused ? 0 : 1;
        LockCursor(!isPaused);
    }

    private void LockCursor(bool locked)
    {
        Cursor.visible = !locked;
        Cursor.lockState = locked ? CursorLockMode.Locked : CursorLockMode.None;
    }

    public void BI_Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }

    public void BI_Exit()
    {
        Application.Quit();
    }

    public void UI_OnToggleChanged()
    {
        Player.Player.immortal = immortalityToggle.isOn;
    }

    public void DisplayDeathMessage()
    {
        deathMessage.SetActive(true);
        immortalityToggle.isOn = true;
        immortalityToggle.interactable = false;
    }
}
