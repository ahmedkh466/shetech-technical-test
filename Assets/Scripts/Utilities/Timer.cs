using UnityEngine;

public class Timer
{
    private double endTime;
    private bool isCounting;

    public Timer()
    {
        isCounting = false;
    }

    public void StartCounting(float period)
    {
        endTime = Time.timeAsDouble + period;
        isCounting = true;
    }
    public bool IsFinished()
    {
        if (!isCounting) return true;
        else
        {
            if (Time.timeAsDouble > endTime)
            {
                isCounting = false;
                return true;
            }
            else return false;
        }
    }
}
