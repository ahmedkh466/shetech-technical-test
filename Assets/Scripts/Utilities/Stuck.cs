using UnityEngine;

public class Stuck : MonoBehaviour
{
    [SerializeField] private Transform T;
    [SerializeField] private Transform anchorT;
    [SerializeField] private bool updateRotation = true;
    [SerializeField] private bool lookAtCamera = true;

    private void Update()
    {
        T.position = anchorT.position;
        if(updateRotation)
            T.rotation = anchorT.rotation;
        if(lookAtCamera)
            T.rotation = Quaternion.LookRotation(T.position - Player.Player.I.cameraT.position);
    }
}
