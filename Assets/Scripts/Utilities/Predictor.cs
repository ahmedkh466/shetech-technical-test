using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Predictor : MonoBehaviour
{
    private Transform target;
    private float time;

    private Vector3[] trajectory;
    private int currentPoint;

    private UnityAction<Vector3> endCall;
    public bool isRecording { get; private set; }

    void Awake()
    {
        trajectory = new Vector3[10];
    }

    private void FixedUpdate()
    {
        if(isRecording)
            Record();
    }

    /// <param name="time">How long into the future is the prediction</param>
    public void Setup(UnityAction<Vector3> endCall, Transform target, float time)
    {
        Clear();
        this.target = target;
        this.time = time;
        this.endCall = endCall;
        isRecording = true;
    }

    public void Clear()
    {
        currentPoint = 0;
        isRecording = false;
    }

    private void Record()
    {
        if (currentPoint < trajectory.Length)
        {
            trajectory[currentPoint] = target.position;
            currentPoint++;
        }
        else
        {
            endCall(PredictBestProjectilePosition(trajectory, time));
            isRecording = false;
        }
    }

    /// <param name="deltaTime">How long it took between each trajectory point was recorded</param>
    public static Vector3 PredictBestProjectilePosition(Vector3[] trajectory, float time)
    {
        Vector3 velocity = Vector3.zero;
        for(int i = 0; i < trajectory.Length-1; i++)
        {
            velocity += (trajectory[i+1] - trajectory[i]) / Time.fixedDeltaTime;
        }
        velocity /= trajectory.Length;

        return trajectory[trajectory.Length-1]+ velocity * time;
    }
}
