using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameObjectPooling
{
    public class Pool : MonoBehaviour
    {
        [SerializeField] private Transform parentT;
        [SerializeField] private GameObject prefab;
        [SerializeField] private int initialNumber;
        private List<Transform> spawnedEntities;

        private void Awake()
        {
            spawnedEntities = new List<Transform>(initialNumber);
            for (int i = 0; i < initialNumber; i++)
            {
                spawnedEntities.Add(SpawnEntity());
            }
        }

        public T SpawnEntity<T>(Vector3 position, Quaternion rotation)
        {
            foreach(Transform entity in spawnedEntities)
            {
                if (!entity.gameObject.activeInHierarchy)
                {
                    entity.position = position;
                    entity.rotation = rotation;
                    entity.gameObject.SetActive(true);
                    return entity.GetComponent<T>();
                }
            }

            Transform newEntity = SpawnEntity();
            spawnedEntities.Add(newEntity);
            newEntity.position = position;
            newEntity.rotation = rotation;
            newEntity.gameObject.SetActive(true);
            return newEntity.GetComponent<T>();
        }

        public void DespawnEntity(Transform entity)
        {
            entity.gameObject.SetActive(false);
        }

        private Transform SpawnEntity()
        {
            Transform entity = Instantiate(prefab, parentT).transform;
            entity.gameObject.SetActive(false);
            return entity;
        }
    }
}