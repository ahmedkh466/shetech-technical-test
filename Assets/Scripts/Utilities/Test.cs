using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Rendering;

public class Test : MonoBehaviour
{
    [SerializeField] private Predictor predictor;
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private float delay;
    [SerializeField] private Transform hitMark;
    private float startTime;
    private bool count;

    private void Awake()
    {
    }

    private void FixedUpdate()
    {
        if (count)
        {
            float timePassed = Time.time - startTime;
            timerText.text = (delay - timePassed).ToString("F2");
            if (delay < timePassed)
            {
                count = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            predictor.Setup(PlaceIt, Player.PlayerController.I.heartT, delay);
            startTime = Time.time;
            count = true;
        }
    }

    public void PlaceIt(Vector3 pos)
    {
        hitMark.position = pos;
    }
}
