using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Boss
{
    public class Target : MonoBehaviour
    {
        private bool fadeAim;
        [SerializeField] private float fadeSpeed;

        public Health health;

        [Header("UI")]
        [SerializeField] private Transform aimT;
        [SerializeField] private Image aimIMG;
        [SerializeField] private Color aimColor;
        private Color targetColor;

        [SerializeField] private float rotationSpeed;
        private float rotationOffset;
        private Quaternion lookAt;

        public void Initialize(int hp)
        {
            health.SetMaxHealth(hp);
            health.Initialize();
            health.EnableBar(true);
            aimT.gameObject.SetActive(true);
            fadeAim = true;
            targetColor = aimColor;
        }

        private void Awake()
        {
            health.Setup(OnDeath);
            rotationOffset = Random.Range(0f, Mathf.PI);
        }

        private void Update()
        {
            if (fadeSpeed != -1)
            {
                lookAt = Quaternion.LookRotation(aimT.position - Player.Player.I.cameraT.position);
                aimT.rotation = lookAt * Quaternion.Euler(Vector3.forward * (Time.time + rotationOffset) * rotationSpeed);
            }

            if (fadeAim)
            {
                aimIMG.color = Color.Lerp(aimIMG.color, targetColor, Time.deltaTime * fadeSpeed);
                if(Mathf.Abs(aimIMG.color.a - targetColor.a) < 0.02f)
                {
                    aimIMG.color = targetColor;
                    fadeAim = false;
                }
            }
        }

        public void OnDeath()
        {
            health.EnableBar(false);
            fadeAim = true;
            targetColor = new Color(0, 0, 0, 0);
        }
    }
}