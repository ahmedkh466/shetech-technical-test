using Boss;
using Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RocksAttack
{
    sealed class RocksAttack : BossAttack
    {
        private RocksAttackParameters parameters;

        [SerializeField] private float timeJumpAndRockShow;
        [SerializeField] private Vector2 rocksHeightInterval;
        [SerializeField] private float rockElevationSpeed;
        private Rock[] rocks;
        /// <summary> "How long the rock will travel space until it hits its target")] </summary>
        private Vector3[] targetPositions;
        private bool allRocksAreUp = false;
        private int currentRock = 0;

        public override void Initiate(object parametersObject) //each attack can have a different damage, attack speed, etc..
        {
            parameters = (RocksAttackParameters)parametersObject;

            rocks = parameters.rocks;
            targetPositions = new Vector3[rocks.Length];
            for (int i = 0; i < rocks.Length; i++)
            {
                rocks[i].Setup();
            }

            //animation
            animator.SetBool("rocks", true);
            animator.SetTrigger("attack"); //jumps in air

            //spawns rocks
            Vector3 temp = Vector3.zero;
            for (int i = 0; i < rocks.Length; i++)
            {
                temp = rocks[i].T.position + Vector3.up * Random.Range(rocksHeightInterval.x, rocksHeightInterval.y);
                targetPositions[i] = temp;
                rocks[i].Initiate(parameters.hitDelay, parameters.damage, parameters.fractionDamage);
                rocks[i].gameObject.SetActive(true);
            }

            allRocksAreUp = false;
            currentRock = 0;
            counter = timeJumpAndRockShow;
        }

        public override void Perform()
        {
            if (!allRocksAreUp) //levetating rocks
            {
                if (counter > 0)
                {
                    if (counter < 0) counter = 0;
                    else counter -= Time.deltaTime;
                }
                else
                {
                    allRocksAreUp = true;

                    Rock rock = null;
                    for (int i = 0; i < rocks.Length; i++)
                    {
                        rock = rocks[i];
                        if (rock.isFloating) continue;

                        rock.T.position = Vector3.Lerp(rock.T.position, targetPositions[i], Time.deltaTime * rockElevationSpeed * (0.5f + 0.5f / (i + 1)));
                        if (Mathf.Abs(rock.T.position.y - targetPositions[i].y) < 1f)
                        {
                            rock.FloatInPlace();
                        }
                        else allRocksAreUp = false;
                    }

                    if (allRocksAreUp)
                    {
                        counter = parameters.attackSpeed;
                    }
                }
            }
            else //all rocks are up
            {
                counter -= Time.deltaTime;
                if (counter < 0)
                {
                    if (currentRock == rocks.Length) //finished all rocks
                    {
                        onAttackEnded.Invoke();
                        End();
                    }
                    else
                    {
                        counter = parameters.attackSpeed;

                        predictor.Setup(rocks[currentRock].Throw, PlayerController.I.heartT, parameters.hitDelay);
                        currentRock++;
                    }
                }
            }
        }

        public override void End()
        {
            Debug.Log("Terminated Rocks Attack");
            animator.SetBool("rocks", false);
            for(int i = 0; i < rocks.Length; i++)
            {
                rocks[i].Terminate();
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (rocks == null) return;

            Gizmos.color = Color.blue;
            for (int i = 0; i < rocks.Length; i++)
            {
                Gizmos.DrawLine(rocks[i].T.position + rocksHeightInterval.x * Vector3.up, rocks[i].T.position + rocksHeightInterval.y * Vector3.up);
            }
        }
    }
}