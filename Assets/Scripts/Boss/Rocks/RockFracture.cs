using UnityEngine;

namespace RocksAttack
{
    public class RockFracture : MonoBehaviour
    {
        [HideInInspector] public Transform T;
        [HideInInspector] public MeshCollider collider;
        [HideInInspector] public Rigidbody rb;
        public int damage;
        private bool hasHitPlayer;

        public void Setup()
        {
            T = transform;
            collider = GetComponent<MeshCollider>();
            rb = GetComponent<Rigidbody>();
        }

        public void ResetFracture()
        {
            collider.isTrigger = false;
            hasHitPlayer = false;
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (hasHitPlayer || rb.velocity.magnitude < 0.5f) return;
            if (collision.gameObject.CompareTag("Player"))
            {
                Debug.Log(damage);
                collision.gameObject.GetComponent<Health>().TakeDamage(damage);
                hasHitPlayer = true;
            }
        }
    }
}