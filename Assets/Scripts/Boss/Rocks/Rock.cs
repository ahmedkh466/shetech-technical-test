using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player;
using System.Linq;

namespace RocksAttack
{
    public class Rock : MonoBehaviour
    {
        public Transform T;
        [SerializeField] private Transform originT;
        private Rigidbody rb;
        private MeshCollider collider;
        private Vector3 rotation;
        /// <summary> How long the rock will travel space until it hits its target </summary>
        private float hitDelay;
        private int damage;

        [Header("Animation")]
        [SerializeField] private Vector2 rotationSpeed;
        [SerializeField] private MeshRenderer energySphere;

        //when rock is floating it just hovers up and down until it is thrown
        [Header("Floating")]
        [SerializeField] private float yMargin;
        public bool isFloating { get; private set; }
        private Vector3 inAirPosition;
        private float counter;
        private bool isBeingThrown;
        private bool turnOffEnergyBall;

        //Fractured Rock
        [Header("Fractured")]
        [SerializeField] private Transform fracturedRockT;
        [SerializeField] private ParticleSystem explosionVFX;
        private RockFracture[] fractures;
        private Vector3[] originalPositions;
        private bool isFractured = false;

        private bool disableAfterTimer;
        private bool piecesHaveFallen;

        private bool isSetup;

        public void Setup()
        {
            T.position = originT.position;
            if (isSetup) return;

            rb = GetComponent<Rigidbody>();
            collider = GetComponent<MeshCollider>();

            fractures = fracturedRockT.GetComponentsInChildren<RockFracture>().Where(t => t != fracturedRockT).ToArray();
            originalPositions = new Vector3[fractures.Length];

            for (int i = 0; i < fractures.Length; i++)
            {
                fractures[i].Setup();
                originalPositions[i] = fractures[i].T.localPosition;
            }

            isSetup = true;

            ResetRock();
        }

        public void Initiate(float time, int damage, int fractionDamage)
        {
            T.position = originT.position;
            ResetRock();
            hitDelay = time;
            this.damage = damage;
            for (int i = 0; i < fractures.Length; i++)
            {
                fractures[i].damage = fractionDamage;
            }
        }

        private void ResetRock()
        {
            T.position = originT.position;

            for (int i = 0; i < fractures.Length; i++)
            {
                fractures[i].T.localPosition = originalPositions[i];
                fractures[i].T.localRotation = Quaternion.Euler(Vector3.right * -90);
                fractures[i].ResetFracture();
            }
            fracturedRockT.gameObject.SetActive(false);

            rotation = new Vector3(Random.Range(rotationSpeed.x, rotationSpeed.y), Random.Range(rotationSpeed.x, rotationSpeed.y), Random.Range(rotationSpeed.x, rotationSpeed.y));

            rb.useGravity = false;
            rb.isKinematic = false;
            collider.isTrigger = true;

            energySphere.material.SetFloat("_FresnelPower", 3.5f);
            energySphere.gameObject.SetActive(true);
            GetComponent<MeshRenderer>().enabled = true;

            isBeingThrown = false;
            isFractured = false;
            isFloating = false;
            piecesHaveFallen = false;
            turnOffEnergyBall = false;
            disableAfterTimer = false;
            counter = 0;
        }

        // Update is called once per frame
        void Update()
        {
            if(!isBeingThrown)
                T.Rotate(rotation * Time.deltaTime * rotationSpeed);

            if (isFloating)
            {
                counter += Time.deltaTime;
                T.position = inAirPosition + Vector3.up * yMargin * Mathf.Sin(counter*1.6f);
            }
            else if (isBeingThrown)
            {
                if (turnOffEnergyBall)
                {
                    float currentPower = energySphere.material.GetFloat("_FresnelPower");
                    energySphere.material.SetFloat("_FresnelPower", Mathf.Lerp(currentPower, 0, Time.deltaTime * 6));
                    if (currentPower < 0.1f)
                    {
                        energySphere.gameObject.SetActive(false);
                        turnOffEnergyBall = false;
                    }
                }
            }

            if (disableAfterTimer)
            {
                counter -= Time.deltaTime;
                if(counter < 0)
                {
                    if (piecesHaveFallen){
                        gameObject.SetActive(false);
                    }
                    else
                    {
                        for(int i = 0; i < fractures.Length; i++)
                        {
                            fractures[i].collider.isTrigger = true;
                        }
                        counter = 2;
                        piecesHaveFallen = true;
                    }
                }
            }
        }

        public void FloatInPlace()
        {
            isFloating = true;
            inAirPosition = T.position;
            counter = 0;
        }

        public void Terminate()
        {
            isFloating = false;
            isBeingThrown = false;
            isBeingThrown = true;
            if (!isFractured)
            {
                Explode();
            }
            if (energySphere.gameObject.activeInHierarchy) energySphere.gameObject.SetActive(false);
        }

        public void Explode()
        {
            if (isFractured) return;

            fracturedRockT.gameObject.SetActive(true);
            GetComponent<MeshRenderer>().enabled = false;
            collider.isTrigger = true;
            for (int i = 0; i < fractures.Length; i++)
            {
                fractures[i].GetComponent<Rigidbody>().velocity = rb.velocity;
            }
            
            rb.AddExplosionForce(10000, T.position, 10);
            rb.isKinematic = true;
            disableAfterTimer = true;
            counter = 3;
            isFractured = true;
            explosionVFX.Play();
        }

        private void OnDrawGizmosSelected()
        {
            if (T == null) return;
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(T.position + Vector3.down * yMargin, 0.3f);
            Gizmos.DrawSphere(T.position + Vector3.up * yMargin, 0.3f);
            Gizmos.DrawLine(T.position + Vector3.down * yMargin, T.position + Vector3.up * yMargin);
        }

        /// <param name="time">After "time" seconds, the rock will reach the target</param>
        public void Throw(Vector3 target)
        {
            isBeingThrown = true;
            turnOffEnergyBall = true;
            isFloating = false;
            collider.isTrigger = false;

            float distance = Vector3.Distance(target, T.position);
            float speed = distance / hitDelay;
            //f = mv/t
            rb.AddForce((rb.mass * speed / hitDelay )* (target - T.position).normalized*55);
            rb.useGravity = true;
            rb.AddTorque(rotationSpeed*10);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (isFractured) return;
            if (collision.transform.CompareTag("Player") && rb.velocity.magnitude > 0.8f)
            {
                collision.gameObject.GetComponent<Health>().TakeDamage(damage);
            }
            Explode();
        }
    }
}