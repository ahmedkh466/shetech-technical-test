using UnityEngine;

namespace RocksAttack
{
    [CreateAssetMenu(fileName = "Rocks Attack", menuName = "Attacks/Rocks Attack")]
    public class RocksAttackParameters : ScriptableObject
    {
        public float attackSpeed;
        public float hitDelay;
        public int damage;
        public int fractionDamage;
        public Rock[] rocks;
    }
}