using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

namespace Boss
{
    public class Boss : MonoBehaviour
    {
        [SerializeField] private BossPhase[] phases;
        private int currentPhaseIndex = -1;
        private BossPhase currentPhase;

        private bool isHpAnimated; // indicates whether or not the hp has completed the animation
        [SerializeField] private float delayBeforePhaseStart;
        private float counter;

        [Header("References")]
        public Transform T;
        [SerializeField] private Animator animator;
        [SerializeField] private Predictor predictor;

        [Header("Effects")]
        public GameObject smokeGroup;

        [Header("Health")]
        [SerializeField] private GameObject healthBarG;
        [SerializeField] private Image healthBar;
        [SerializeField] private float barDamp;
        private int phaseHealth;

        [Header("UI")]
        [SerializeField] private TextMeshProUGUI phaseText;
        [SerializeField] private GameObject startingMessage;

        private void Start()
        {
            for(int i = 0; i < phases.Length; i++)
            {
                phases[i].Setup(animator, predictor);
            }
            healthBarG.SetActive(false);
        }

        private void Update()
        {
            if (currentPhaseIndex == -1)
            {
                if (Input.GetKeyDown(KeyCode.P))
                {
                    StartFighting();
                    startingMessage.SetActive(false);
                }
            }
            else
            {
                LookAtPlayer();
                if (Input.GetKeyDown(KeyCode.Keypad0))
                {
                    StartNextPhase();
                }

                phaseHealth = currentPhase.GetHealth();
                UpdateHealthBar();

                if(phaseHealth == 0)
                {
                    if (currentPhaseIndex != phases.Length - 1)
                        StartNextPhase();
                    else
                        phaseText.text = "Battle is finished!";
                }
                else
                {
                    HandlePhase();
                }
            }
        }

        private void HandlePhase()
        {
            if (isHpAnimated)
            {
                currentPhase.Perform();
            }
            else
            {
                counter -= Time.deltaTime;
                if (counter < 0)
                {
                    isHpAnimated = true;
                }
            }
        }

        private void StartNextPhase()
        {
            if (currentPhaseIndex != phases.Length - 1)
                StartPhase(currentPhaseIndex + 2);
            else StartPhase(1);
        }

        private void UpdateHealthBar()
        {
            if(isHpAnimated)
                healthBar.fillAmount = Mathf.Lerp(healthBar.fillAmount, phaseHealth / (float)currentPhase.maxHealth, Time.deltaTime * barDamp); 
            else
                healthBar.fillAmount = Mathf.Lerp(healthBar.fillAmount, 1, Time.deltaTime * barDamp);
        }

        private void StartFighting()
        {
            healthBar.fillAmount = 0;
            healthBarG.SetActive(true);
            StartPhase(1);
        }

        /// <param name="phaseNumber">1, 2, 3 or 4</param>
        private void StartPhase(int phaseNumber)
        {
            if(currentPhase != null)
                currentPhase.End();
            currentPhaseIndex = phaseNumber - 1;
            currentPhase = phases[currentPhaseIndex];
            currentPhase.InitiatePhase();

            isHpAnimated = false;
            counter = delayBeforePhaseStart;
            phaseText.text = "Phase " + phaseNumber;
        }

        private void LookAtPlayer()
        {
            Vector3 playerPos = PlayerController.I.T.position;
            playerPos.y = T.position.y;
            if (Vector3.Distance(playerPos, T.position) > 0.5f)
                T.rotation = Quaternion.LookRotation(playerPos - T.position);
        }
        
        public void ANIM_PlaySmoke()
        {
            smokeGroup.SetActive(false);
            smokeGroup.SetActive(true);
        }

        public void BI_SetPhase(int phase)
        {
            if (phase - 1 == currentPhaseIndex) return;

            StartPhase(phase);
        }
    }
}