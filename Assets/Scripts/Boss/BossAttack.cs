using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using static Boss.BossAttack;

namespace Boss
{
    public class BossAttack : MonoBehaviour
    {
        protected Animator animator;
        protected Predictor predictor;
        protected UnityAction onAttackEnded;
        protected float counter;

        public void Setup(UnityAction onAttackEnded, Animator bossAnimator, Predictor predictor)
        {
            animator = bossAnimator;
            this.predictor = predictor;
            this.onAttackEnded = onAttackEnded;
        }

        public virtual void Initiate(object settings)
        {

        }

        public virtual void Perform()
        {

        }

        public virtual void End()
        {

        }
    }
}