using Player;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Boss
{
    public class Lazer : MonoBehaviour
    {
        public bool isOn { get; private set; }
        public bool finishedAiming { get; private set; }

        [SerializeField] private Transform lazerOriginT;
        [SerializeField] private GameObject lazerLine;
        [SerializeField] private Transform lazerT;
        private float counter;
        [SerializeField] private float shakeSpeed;
        [SerializeField] private int nShakes;
        [SerializeField] private Transform[] lazerRings;
        [SerializeField] private float refocusDelay;
        [SerializeField] private float lazerRotationDamp;
        [SerializeField] private float aimingTimeout;

        private Timer aimingTimer = new Timer();
        private float lazerFinalWidth;
        private bool wait;
        private int i;
        private Vector3 lazerOffset;

        private void Awake()
        {
            lazerFinalWidth = lazerT.localScale.x;
        }

        private void Update()
        {
            if (isOn)
            {
                AimLazer();
            }
        }

        public void ActivateLazer()
        {
            lazerOffset = Vector3.zero;
            lazerT.rotation = Quaternion.LookRotation(PlayerController.I.T.position + Vector3.up * 1.5f - lazerOriginT.position);
            for (int i = 0; i < lazerRings.Length; i++)
            {
                lazerRings[i].gameObject.SetActive(false);
            }
            lazerT.gameObject.SetActive(true);
            wait = true;
            counter = shakeSpeed / 2;
            i = 0;
            isOn = true;
            finishedAiming = false;
        }

        private void DeactiveLazer()
        {
            lazerT.gameObject.SetActive(false);
            isOn = false;
        }

        private void AimLazer()
        {
            Vector3 lazerTarget = PlayerController.I.T.position + Vector3.up * 1.5f + lazerOffset;
            Vector3 difference = lazerOriginT.position - lazerTarget;
            lazerT.position = lazerOriginT.position;
            float w = lazerFinalWidth + lazerFinalWidth * (1f - ((i + 1f) / nShakes));
            lazerT.localScale = new Vector3(w, w, difference.magnitude + 4);
            lazerT.rotation = Quaternion.Lerp(lazerT.rotation, Quaternion.LookRotation(lazerTarget - lazerOriginT.position), Time.deltaTime * lazerRotationDamp);

            if (i < nShakes)
            {
                AnimateLazerInitiation();
                return;
            }

            counter -= Time.deltaTime;
            if (counter < 0)
            {
                counter = refocusDelay;
                lazerOffset = new Vector3(Random.Range(-1, 1f), 0, Random.Range(-1, 1f));
            }

            lazerRings[0].localPosition = Vector3.forward * (0.04f + 0.05f * (Mathf.Sin(Time.time) * 0.5f + 0.5f));
            lazerRings[1].localPosition = Vector3.forward * (0.1f + 0.1f * (Mathf.Sin(Time.time) * 0.5f + 0.5f));
            lazerRings[2].localPosition = Vector3.forward * (0.2f + 0.2f * (Mathf.Sin(Time.time) * 0.5f + 0.5f));

            if (aimingTimer.IsFinished())
            {
                finishedAiming = true;
                DeactiveLazer();
            }
        }

        private void AnimateLazerInitiation()
        {
            if (wait)
            {
                counter -= Time.deltaTime;
                if (counter < 0)
                {
                    lazerLine.gameObject.SetActive(false);
                    counter = shakeSpeed / (Mathf.Pow(2, i));
                    wait = false;
                }
            }
            else
            {
                counter -= Time.deltaTime;
                if (counter < 0)
                {
                    i++;
                    if (i == nShakes)
                    {
                        lazerLine.gameObject.SetActive(true);
                        for (int i = 0; i < lazerRings.Length; i++)
                        {
                            lazerRings[i].gameObject.SetActive(true);
                        }
                        aimingTimer.StartCounting(aimingTimeout);
                        counter = 0;
                    }
                    else
                    {
                        counter = shakeSpeed / 2;
                        lazerLine.gameObject.SetActive(true);
                        wait = true;
                    }
                }
            }
        }
    }
}