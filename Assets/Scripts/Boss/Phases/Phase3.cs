using RocksAttack;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boss
{
    public class Phase3 : BossPhase
    {
        [SerializeField] private Rock[] rocksGroup1;
        [SerializeField] private Rock[] rocksGroup2;

        public override void Setup(Animator bossAnimator, Predictor bossPredictor)
        {
            bool firstWasAssigned = false;
            for (int i = 0; i < parameters.Length; i++)
            {
                if (parameters[i] is RocksAttackParameters)
                {
                    if (!firstWasAssigned)
                    {
                        ((RocksAttackParameters)parameters[i]).rocks = rocksGroup1;
                        firstWasAssigned = true;
                    }
                    else
                    {
                        ((RocksAttackParameters)parameters[i]).rocks = rocksGroup2;
                        break;
                    }
                }
            }

            base.Setup(bossAnimator, bossPredictor);
        }

        public override void InitiatePhase()
        {
            base.InitiatePhase();

            InitiateAttack(0);
        }
    }
}