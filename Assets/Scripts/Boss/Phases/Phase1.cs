using RocksAttack;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boss
{
    public class Phase1 : BossPhase
    {
        [SerializeField] private Rock[] rocksGroup;

        public override void Setup(Animator bossAnimator, Predictor bossPredictor)
        {
            for(int i = 0; i < parameters.Length; i++)
            {
                if (parameters[i] is RocksAttackParameters)
                {
                    ((RocksAttackParameters)parameters[i]).rocks = rocksGroup;
                }
            }

            base.Setup(bossAnimator, bossPredictor);
        }

        public override void InitiatePhase()
        {
            base.InitiatePhase();

            InitiateAttack(0);
        }
    }
}