using Boss;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boss
{
    public class Phase4 : BossPhase
    {
        [SerializeField] private Lazer lazer;

        public override void InitiatePhase()
        {
            base.InitiatePhase();

            InitiateAttack(0);
            lazer.ActivateLazer();
        }
    }
}