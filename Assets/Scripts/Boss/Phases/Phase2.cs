using RocksAttack;
using SpeediesAttack;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boss {
    public class Phase2 : BossPhase
    {
        public override void InitiatePhase()
        {
            base.InitiatePhase();

            InitiateAttack(0);
        }
    }
}