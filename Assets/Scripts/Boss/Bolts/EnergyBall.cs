using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boss
{
    public class EnergyBall : MonoBehaviour
    {
        [SerializeField] private Transform T;
        [SerializeField] private GameObject impactVFX;
        [SerializeField] private int damage;
        private Transform target;
        private Vector3 velocity;
        private Vector3 accelerationVector;
        private float acceleration; //this affects only the direction, works like centripetal acceleration
        private float speed;

        public void Setup(float speed, float acceleration, Vector3 initialVelocity, Transform target)
        {
            this.target = target;
            this.speed = speed;
            velocity = initialVelocity;
            this.acceleration = acceleration;
        }

        private void FixedUpdate()
        {
            if(Vector3.Distance(T.position, target.position) > 2) {
                accelerationVector = (target.position - T.position).normalized * acceleration;
                velocity = (velocity + accelerationVector * Time.fixedDeltaTime).normalized * speed;
            }
            
            T.position += velocity * Time.fixedDeltaTime;
        }

        private void OnTriggerEnter(Collider collision)
        {
            Destroy(Instantiate(impactVFX, T.position, T.rotation), 4);
            if (collision.CompareTag("Player"))
            {
                collision.gameObject.GetComponent<Health>().TakeDamage(damage);
            }
            gameObject.SetActive(false);
        }
    }
}