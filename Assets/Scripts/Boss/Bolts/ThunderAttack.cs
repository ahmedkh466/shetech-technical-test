using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Boss;
using GameObjectPooling;

namespace BoltsAttack {
    sealed class ThunderAttack : BossAttack
    {
        private BoltsAttackParameters parameters;

        [SerializeField] private LayerMask groundLayer;
        private Ray ray;
        private RaycastHit hit;

        [Header("Bolts")]
        [SerializeField] private Bolt[] bolts;
        private int nBolts;
        private int boltsCounter;

        [Header("Bullets")]
        [SerializeField] private Pool bulletsPool;
        [SerializeField] private Transform bulletSpawnPosition1;
        [SerializeField] private Transform bulletSpawnPosition2;
        [SerializeField] private float bulletAcceleration;
        /// <summary> next spawn position </summary>
        private bool nextIsLeft;
        private float bulletTimer;

        public override void Initiate(object parametersObject)
        {
            parameters = (BoltsAttackParameters)parametersObject;
            //animation
            animator.SetBool("bolts", true);

            nBolts = Random.Range(parameters.minBolts, parameters.maxBolts);
            boltsCounter = 0;
            for (int i = 0; i < bolts.Length; i++)
            {
                bolts[i].Setup(parameters.boltChargingTime, parameters.boltDamage);
            }
        }

        public override void Perform()
        {
            //bullets
            bulletTimer -= Time.deltaTime;
            if (bulletTimer < 0)
            {
                bulletTimer = parameters.bulletAttackSpeed;
                ShootBullet();
            }

            //bolts
            counter -= Time.deltaTime;
            if(counter < 0)
            {
                PrepareBoltForCasting();

                boltsCounter++;
                if (boltsCounter == nBolts)
                {
                    onAttackEnded();
                    End();
                }
                else
                    counter = parameters.boltAttackSpeed;
            }
        }

        private void ShootBullet()
        {
            EnergyBall bullet = bulletsPool.SpawnEntity<EnergyBall>(nextIsLeft ? bulletSpawnPosition2.position : bulletSpawnPosition1.position, 
                Quaternion.identity);
            bullet.Setup(parameters.bulletMovementSpeed, bulletAcceleration, 
                Vector3.up * parameters.bulletMovementSpeed, Player.PlayerController.I.heartT);
        }

        public override void End()
        {
            Debug.Log("Bolts attack Ended");
            animator.SetBool("bolts", false);
        }

        private void PrepareBoltForCasting()
        {
            predictor.Setup(CastBolt, Player.PlayerController.I.T, parameters.boltChargingTime);
        }

        public void CastBolt(Vector3 position)
        {
            Vector3 point = Player.PlayerController.I.T.position, normal = Vector3.up;

            ray.origin = position + Vector3.up * 3;
            ray.direction = Vector3.down;
            if (Physics.Raycast(ray, out hit, 1000, groundLayer))
            {
                normal = hit.normal;
                point = hit.point;
            }

            GetAvailableBolt().HitAt(point, normal);
            animator.SetTrigger("attack");
        }

        private Bolt GetAvailableBolt()
        {
            for(int i = 0;i< bolts.Length; i++)
            {
                if (bolts[i].isFinished) return bolts[i];
            }
            return bolts[0];
        }
    }
}