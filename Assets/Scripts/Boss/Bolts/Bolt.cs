using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoltsAttack
{
    public class Bolt : MonoBehaviour
    {
        private bool exploded = false;
        private float counter;
        public bool isFinished { get; private set; }
        private int damage;
        [SerializeField] private float radius;

        [SerializeField] private Transform T;
        [SerializeField] private GameObject explosion;
        [SerializeField] private GameObject charge;
        [SerializeField] private Transform circlesT;
        [SerializeField] private float delayToDeactivate;
        private float chargingTime;
        private Vector3 circlesLocalScale;

        public void Setup(float chargingTime, int damage)
        {
            this.chargingTime = chargingTime;
            gameObject.SetActive(false);
            isFinished = true;
            this.damage = damage;
        }

        private void Awake()
        {
            circlesLocalScale = circlesT.localScale;
        }

        private void Update()
        {
            if (isFinished) return;

            if (exploded)
            {
                counter -= Time.deltaTime;
                if (counter < 0)
                {
                    gameObject.SetActive(false);
                    isFinished = true;
                    counter = 0;
                }
            }
            else
            {
                circlesT.localScale = Vector3.Lerp(circlesT.localScale, circlesLocalScale * 0.1f, Time.deltaTime * 2);
                if (counter < 0)
                {
                    charge.SetActive(false);
                    explosion.SetActive(true);
                    exploded = true;
                    counter = delayToDeactivate;
                    if(Physics.CheckSphere(T.position, radius, Player.Player.I.layer))
                    {
                        Player.Player.I.GetComponent<Health>().TakeDamage(damage);
                    }
                }
                else if (counter > 0) counter -= Time.deltaTime;
            }
        }

        public void HitAt(Vector3 position, Vector3 groundNormal)
        {
            exploded = false;
            isFinished = false;
            counter = chargingTime;

            T.position = position;
            T.rotation = Quaternion.LookRotation(groundNormal);

            explosion.SetActive(false);
            charge.SetActive(true);
            circlesT.localScale = circlesLocalScale;

            gameObject.SetActive(true);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(T.position, radius);
        }
    }
}