using UnityEngine;

namespace BoltsAttack
{
    [CreateAssetMenu(fileName = "Bolts Attack", menuName = "Attacks/Bolts Attack")]
    public class BoltsAttackParameters : ScriptableObject
    {
        public float boltAttackSpeed;
        public float boltChargingTime;
        public int boltDamage;
        public int maxBolts;
        public int minBolts;

        public float bulletMovementSpeed;
        public float bulletAttackSpeed;
    }
}