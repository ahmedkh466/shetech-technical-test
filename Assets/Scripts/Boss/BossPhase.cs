using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Boss
{
    public class BossPhase : MonoBehaviour
    {
        public bool isActive { get; private set; }

        protected Animator bossAnimator;
        protected Predictor bossPredictor;

        [SerializeField] protected BossAttack[] attacks;
        [SerializeField] protected ScriptableObject[] parameters;
        protected BossAttack currentAttack;
        private int attackIndex = 0;

        [SerializeField] private float delayBetweenAttacks;
        private bool loadingTheNextAttack;
        private float counter;

        [Header("Boss Health")]
        public Target[] targets;
        [SerializeField] protected int[] health;
        public int maxHealth { get; private set; }

        /// <summary>
        /// This is called once in the game for none-changing data
        /// </summary>
        /// <param name="bossAnimator"></param>
        /// <param name="bossPredictor"></param>
        public virtual void Setup(Animator bossAnimator, Predictor bossPredictor)
        {
            this.bossAnimator = bossAnimator;
            this.bossPredictor = bossPredictor;
        }

        /// <summary>
        /// this is called to start the phase
        /// </summary>
        public virtual void InitiatePhase()
        {
            for (int i = 0; i < attacks.Length; i++)
            {
                attacks[i].Setup(OnAttackEnded, bossAnimator, bossPredictor);
            }
            maxHealth = 0;
            for (int i = 0; i < targets.Length; i++)
            {
                targets[i].Initialize(health[i]);
                maxHealth += targets[i].health.health;
            }
            isActive = true;
        }

        protected void InitiateAttack(int index)
        {
            attackIndex = index;
            currentAttack = attacks[index];
            currentAttack.Initiate(parameters[index]);
            Debug.Log("Initiated attack " + currentAttack.gameObject.name);
        }

        public virtual void End()
        {
            currentAttack.End();
            Debug.Log("Phase End Call so " + currentAttack.gameObject.name + " was terminated");
            isActive = false;
        }

        public void Perform()
        {
            if (loadingTheNextAttack)
            {
                counter -= Time.deltaTime;
                if (counter < 0)
                {
                    InitiateAttack(attackIndex);
                    loadingTheNextAttack = false;
                }
            }
            else
            {
                currentAttack.Perform();
            }
        }

        public virtual void OnAttackEnded()
        {
            if(isActive)
                NextAttack();
        }

        private void NextAttack()
        {
            if (attackIndex < attacks.Length - 1)
            {
                attackIndex++;
            }
            else attackIndex = 0;
            loadingTheNextAttack = true;
            counter = delayBetweenAttacks;
        }

        public int GetHealth()
        {
            int hp = targets[0].health.health;
            for(int i = 1; i < targets.Length; i++)
            {
                hp += targets[i].health.health;
            }
            return hp;
        }
    }
}