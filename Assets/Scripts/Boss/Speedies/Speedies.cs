using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Boss;

namespace SpeediesAttack
{
    public class Speedies : BossAttack
    {
        private SpeediesAttackParameters parameters;

        [SerializeField] private SpawnCircle[] spawns;
        [SerializeField] private float chargingTime;

        public override void Initiate(object parametersObject)
        {
            parameters = (SpeediesAttackParameters)parametersObject;

            for (int i = 0; i < parameters.nSpawns; i++)
            {
                spawns[i].speedy.SetParameters(parameters.punchDamage, parameters.punchCriticalDamage, parameters.punchCriticalChance,
                    parameters.kickDamage, parameters.kickCriticalDamage, parameters.kickCriticalChance);
                spawns[i].StartCharging(chargingTime);
            }
        }

        public override void End()
        {
            base.End();
            for (int i = 0; i < spawns.Length; i++)
            {
                if (!spawns[i].hasSpawnedSpeedy)
                    spawns[i].Terminate();
            }
        }

        public override void Perform()
        {
            for(int i = 0; i < parameters.nSpawns; i++)
            {
                if (!spawns[i].hasSpawnedSpeedy || !spawns[i].speedy.isDead) {
                    return;
                }
            }

            //all are dead
            onAttackEnded();
        }
    }
}