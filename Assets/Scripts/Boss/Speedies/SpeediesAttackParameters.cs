using UnityEngine;

namespace SpeediesAttack
{
    [CreateAssetMenu(fileName = "Speedies Attack", menuName = "Attacks/Speedies Attack")]
    public class SpeediesAttackParameters : ScriptableObject
    {
        [Header("Punch")]
        public int punchDamage;
        public int punchCriticalDamage;
        [Range(0, 100)] public int punchCriticalChance;

        [Header("Kick")]
        public int kickDamage;
        public int kickCriticalDamage;
        [Range(0, 100)] public int kickCriticalChance;

        public int nSpawns;
    }
}